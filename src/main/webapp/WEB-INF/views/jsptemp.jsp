<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2024/4/15
  Time: 10:07
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="shortcut icon" href="/static/icon/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
</head>
<body>
<table class="table">
    <tr>
        <td width="100">图片</td>
        <td width="120">作者</td>
        <td width="200">教程名称</td>
        <td width="300">内容</td>
    </tr>
    <c:forEach var="article" items="${articles}">
        <tr class="text-info">
            <td><img src="/static/image/jsp.png" height="80"></td>
            <td>${article.author}</td>
            <td>${article.title}</td>
            <td>${article.content}</td>
        </tr>
    </c:forEach>


</table>


<script src="/static/jquery/jquery.min.js "></script>
<script src="/static/bootstrap/js/bootstrap.min.js"></script>
</body>