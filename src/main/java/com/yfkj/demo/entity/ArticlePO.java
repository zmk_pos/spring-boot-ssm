package com.yfkj.demo.entity;


import com.baomidou.mybatisplus.annotation.KeySequence;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;

import java.io.Serializable;

/**
 * 测试-文章 DO
 *
 * @author 远方科技
 */
@TableName("test_article")
@KeySequence("test_article_seq") // 用于 Oracle、PostgreSQL、Kingbase、DB2、H2 数据库的主键自增。如果是 MySQL 等数据库，可不写。
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ArticlePO implements Serializable {

    /**
     * 编号
     */
    @TableId
    private Integer id;
    /**
     * 标题
     */
    private String title;
    /**
     * 作者
     */
    private String author;
    /**
     * 是否显示：0隐藏1显示
     */
    private Boolean hasShow;
    /**
     * 内容
     */
    private String content;
//    /**
//     * 创建者名称
//     */
//    private String creatorName;
//    /**
//     * 是否删除
//     */
//    private Integer deleted;
//    /**
//     * 创建时间
//     */
//    private Date createTime;
//    /**
//     * 创建者，目前使用 SysUser 的 id 编号
//     */
//    private String creator;
//    /**
//     * 修改时间
//     */
//    private Date updateTime;
//    /**
//     * 更新者，目前使用 SysUser 的 id 编号
//     */
//    private String updater;

}
