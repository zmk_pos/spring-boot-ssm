package com.yfkj.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yfkj.demo.entity.ArticlePO;
import com.yfkj.demo.vo.ArticleVO;

import java.util.List;

/**
 * @author zmk
 * @description 功能描述
 * @create 2024/4/15 10:00
 */
public interface ArticleService  extends IService<ArticlePO> {
    List<ArticlePO> getAll();
}
