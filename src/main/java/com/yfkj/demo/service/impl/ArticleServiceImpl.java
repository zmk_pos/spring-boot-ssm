package com.yfkj.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yfkj.demo.entity.ArticlePO;
import com.yfkj.demo.dao.ArticleMapper;
import com.yfkj.demo.service.ArticleService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zmk
 * @description 功能描述
 * @create 2024/4/15 10:00
 */
@Slf4j
@Service
@Validated
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, ArticlePO> implements ArticleService {

    @Resource
    private ArticleMapper articleMapper;

    @Override
    public List<ArticlePO> getAll() {
        return articleMapper.selectList(null);

    }
}
