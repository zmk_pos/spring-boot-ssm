package com.yfkj.demo.config;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author zmk
 * @description 功能描述
 * @create 2024/4/16 9:42
 */
@SuppressWarnings("SpringComponentScan")
@Configuration
@MapperScan(basePackages = {"${yfkj.info.base-package}.dao"})
public class MybatisPlusConfig {
    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        //新的分页插件配置方法（Mybatis Plus 3.4.0版本及其之后的版本）
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return mybatisPlusInterceptor;
    }

}

