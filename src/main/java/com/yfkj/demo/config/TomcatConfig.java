//package com.yfkj.demo.config;
//
//import org.apache.catalina.Context;
//import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
//import org.springframework.boot.web.servlet.ServletContextInitializer;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
///**
// * @author zmk
// * @description 功能描述
// * @create 2024/4/16 11:48
// */
//@Configuration
//public class TomcatConfig {
//    @Bean
//    public TomcatServletWebServerFactory servletWebServerFactory() {
//        return new TomcatServletWebServerFactory() {
//
//            @Override
//            protected void configureContext(Context context, ServletContextInitializer[] initializers) {
//                // jsp文件所在路径
//                context.setDocBase("D:\\project\\yfkj-oa-demo\\target\\war");
//                super.configureContext(context, initializers);
//            }
//        };
//    }
//}