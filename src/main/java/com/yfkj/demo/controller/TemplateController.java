package com.yfkj.demo.controller;

import com.yfkj.demo.entity.ArticlePO;
import com.yfkj.demo.service.ArticleService;
import com.yfkj.demo.vo.ArticleVO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author zmk
 * @description 功能描述
 * @create 2024/4/15 9:59
 */
@Controller
@RequestMapping("temp")
public class TemplateController {

    @Resource
    ArticleService articleService;

    @GetMapping("/jsp")
    public String index(String name, Model model) {

        List<ArticlePO> articles = articleService.getAll();

        model.addAttribute("articles", articles);

        //模版名称，实际的目录为：src/main/webapp/WEB-INF/jsp/jsptemp.jsp
        return "jsptemp";
    }
}