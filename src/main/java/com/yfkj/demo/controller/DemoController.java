package com.yfkj.demo.controller;

import com.yfkj.demo.entity.ArticlePO;
import com.yfkj.demo.framework.common.po.ApiResult;
import com.yfkj.demo.service.ArticleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;


/**
 * @author zmk
 * @description 功能描述
 * @create 2024/4/15 9:59
 */
@RestController
@RequestMapping("/demo")
public class DemoController {

    @Resource
    ArticleService articleService;

    @GetMapping("/get/list")
    public ApiResult<List<ArticlePO>> index() {
        return ApiResult.success(articleService.getAll());
    }
}