package com.yfkj.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yfkj.demo.entity.ArticlePO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author zmk
 * @description 功能描述
 * @create 2024/4/15 10:01
 */
@Mapper
public interface ArticleMapper extends BaseMapper<ArticlePO> {
}
