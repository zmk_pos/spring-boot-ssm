package com.yfkj.demo.framework.common.po;

import com.yfkj.demo.common.exception.ErrorCode;
import com.yfkj.demo.common.exception.ExceptionCode;

import java.io.Serializable;

/**
 * @author zmk
 * @description 功能描述
 * @create 2024/4/16 13:34
 */
public class ApiResult<T> implements Serializable {

    private int code;   //返回码 非0即失败
    private String msg; //消息提示
    private T data; //返回的数据

    public ApiResult() {
    }

    public ApiResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
        this.data = null;
    }

    public ApiResult(int code, T data) {
        this.code = code;
        this.data = data;
    }

    public ApiResult(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ApiResult(ExceptionCode exceptionCode, T data) {
        this.code = exceptionCode.value();
        this.data = data;
    }


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static <T> ApiResult<T> success(T data) {
        return new ApiResult(ExceptionCode.Normal, data);
    }


    public ApiResult error(ExceptionCode exceptionCode, T data) {
        return new ApiResult(exceptionCode, data);
    }


    public static <T> ApiResult<T> error(String message) {
        return error(ExceptionCode.DATA_ERROR.value(), message);
    }


    public static <T> ApiResult<T> error(ErrorCode errorCode) {
        return error(errorCode.getCode(), errorCode.getMsg());
    }

    public static <T> ApiResult<T> error(Integer code, String message) {
        ApiResult<T> result = new ApiResult<>();
        result.code = code;
        result.msg = message;
        return result;
    }
}