//package com.yfkj.demo.listener;
//
//import javax.servlet.ServletContext;
//import javax.servlet.ServletContextEvent;
//import javax.servlet.ServletContextListener;
//import javax.servlet.annotation.WebListener;
//
//`
///**
// * 动态创建servlet、filter、listener
// * @author zmk
// * @description 功能描述
// * @create 2024/4/16 11:47
// */
//@WebListener()
//public class DynRegListener implements ServletContextListener {
//
//    @Override
//    public void contextInitialized(ServletContextEvent sce) {  // ServletContext 创建时，容器调用该方法
//        // 通过ServletContext 对象就可以动态创建servlet、filter、listener
//        ServletContext servletContext = sce.getServletContext();
//    }
//}