package com.yfkj.demo.framework.handler;

import org.jetbrains.annotations.NotNull;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.yfkj.demo.framework.common.po.ApiResult;

import javax.annotation.Resource;

/**
 * @author zmk
 * @description 功能描述
 * @create 2024/4/16 13:42
 */
@ControllerAdvice
public class ResponseBodyHandler implements ResponseBodyAdvice<Object> {
    @Override
    public boolean supports(MethodParameter methodParameter, Class<? extends HttpMessageConverter<?>> aClass) {
        //        System.out.println("=====执行ResponseDataHandler-supports=====");
        //使用此方法,則controller裡面的方法必須加上@ResponseBody才會返回true
        //return methodParameter.hasMethodAnnotation(ResponseBody.class);
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object obj, MethodParameter methodParameter, MediaType mediaType, Class<? extends HttpMessageConverter<?>> aClass, ServerHttpRequest serverHttpRequest, ServerHttpResponse serverHttpResponse) {
//        String uri = serverHttpRequest.getURI().toString();
        if (methodParameter.hasMethodAnnotation(ExceptionHandler.class)) {
            return obj;
        } else if (obj instanceof Resource || obj instanceof ApiResult) {
            return obj;
        }
        return obj;
    }

}
